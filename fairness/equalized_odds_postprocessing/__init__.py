import sklearn
from sklearn.model_selection import GridSearchCV, train_test_split
from fairlearn.postprocessing import ThresholdOptimizer
from sklearn.metrics import accuracy_score, confusion_matrix
import pandas as pd
import numpy as np

def _return_best_model(grid_search: GridSearchCV, X_train: pd.DataFrame, y_train: pd.Series):
    best_model = grid_search.fit(X_train, y_train)
    return best_model.best_estimator_, best_model.best_params_


def fair_classifier(dataset: pd.DataFrame, model: sklearn.base.BaseEstimator, model_params: dict, output_column: str, protected_attributes):
    X = dataset.drop(columns=[output_column], inplace=False)
    y = dataset[output_column]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
    grid_search = GridSearchCV(model, model_params, scoring='accuracy', cv=10)
    best_model, best_params = _return_best_model(grid_search, X_train, y_train)
    
    print('Best params: ', best_params)

    threshold_optimizer = ThresholdOptimizer(
        estimator=best_model,
        constraints='equalized_odds',
        objective='accuracy_score'
    )

    threshold_optimizer.fit(X_train, y_train, sensitive_features=X_train[protected_attributes])
    y_pred = threshold_optimizer.predict(X_test, sensitive_features=X_test[protected_attributes])

    accuracy = accuracy_score(y_test, y_pred)

    return accuracy




